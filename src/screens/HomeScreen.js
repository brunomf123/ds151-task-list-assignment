import React from 'react';
import { View, Button } from 'react-native';

function HomeScreen({ navigation }) {
	return (
		<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
			<Button
				title="Lista de atividades a fazer..."
				onPress={() => navigation.navigate('Tasks')}
			/>
		</View>
	);
}

export default HomeScreen;