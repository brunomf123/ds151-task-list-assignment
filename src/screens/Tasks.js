import React, { useState } from 'react';
import { View, TextInput, StyleSheet, FlatList } from 'react-native';
import ChangeText from '../components/ChangeText';

const Tasks = () => {
	const [text, setText] = useState('');
	const [toDo, setToDo] = useState([]);
	const [comp, setComp] = useState([]);

	function addText(e) {
		setToDo([...toDo, text]);
		setText('');
	}

	const changeToDo = (nome) => {
		setToDo(toDo.filter(item => item !== nome))
		setComp([...comp, nome])
	}

	const changecomp = (nome) => {
		setComp(comp.filter(item => item !== nome))
		setToDo([...toDo, nome])
	}

	return (
		<View>
			<TextInput
				autoCapitalize="none"
				autoCorrect={false}
				placeholder="Nova Tarefa"
				value={text}
				style={styles.textInput}
				returnKeyType="done"
				onChangeText={(t) => setText(t)}
				onSubmitEditing={(e) => addText(e)}
			/>
			<FlatList
				data={toDo}
				keyExtractor={item => item}
				renderItem={({ item }) => <ChangeText text={item} style={styles.textAdded} change={changeToDo} />}
			/>
			<FlatList
				data={comp}
				keyExtractor={item => item}
				renderItem={({ item }) => <ChangeText text={item} style={styles.textCompl} change={changecomp} />}
			/>

		</View>
	)
}

const styles = StyleSheet.create({
	textInput: {
		borderWidth: 1,
		borderColor: 'black',
		margin: 10,
		padding: 5,
	},
	textAdded: {
		backgroundColor: 'lightblue',
		fontSize: 30,
		margin: 5,
		padding: 5,

	},
	textCompl: {
		backgroundColor: 'gray',
		fontSize: 30,
		margin: 5,
		padding: 5,
		textDecorationLine: 'line-through',
	}
});

export default Tasks;