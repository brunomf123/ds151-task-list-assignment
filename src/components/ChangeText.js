import React from 'react';
import { Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const ChangeText = ({ text, style, change }) => {
	return (
		<TouchableOpacity>
			<Text style={style} onPress={() => change(text)}>
				{text}
			</Text>
		</TouchableOpacity>
	)
}

export default ChangeText;